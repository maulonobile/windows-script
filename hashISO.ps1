# Calcola hash file con powershell
# Utilizzo: passare il nome del file come argomento
# esempio: ./hashISO-powershell.ps1 file.iso
Get-FileHash $args[0] -Algorithm SHA512
Get-FileHash $args[0] -Algorithm SHA256
Get-FileHash $args[0] -Algorithm MD5
